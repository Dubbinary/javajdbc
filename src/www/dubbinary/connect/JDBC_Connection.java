package www.dubbinary.connect;

import java.io.InputStream;
import java.io.OutputStream;
import java.sql.*;

/**
 * Created by dubbinary on 20.02.15.
 */
public class JDBC_Connection {

    private final static String NO_JDBC_DRIVER = "[ERROR]: NO JDBC Driver avaliable";
    private final static String SQL_EXEPTION = "[ERROR]: SQL Exeption: ";
    private final static String OK_RESULT = "[OK]: ";
    private final static String STATUS = "[STATUS]: ";

    private Connection connection = null;
    private Statement statement = null;
    private PreparedStatement prepared_statement = null;
    private ResultSet result_set = null;

    private String url;
    private String user;
    private String password;

    private InputStream input = System.in;
    private OutputStream output = System.out;

    public JDBC_Connection(String url, String user, String password){
        this.url = url;
        this.user = user;
        this.password = password;
    }

    public Connection getConnection(){
        try{
            Class.forName("com.mysql.jdbc.Driver");
            System.out.println(STATUS+"\t Establishing conection to database...");
            connection = DriverManager.getConnection(url, user, password);
            System.out.println(OK_RESULT+"\t Connection established");
        }
        catch (ClassNotFoundException e) {
            System.out.println(NO_JDBC_DRIVER);
            return null;
        }
        catch(SQLException ex){
            System.out.println(SQL_EXEPTION+"\n\t Can`t get connection");
        }
        return connection;
    }

    public Statement getStatement(){
        try {
            return connection.createStatement();
        } catch (SQLException e) {
            System.out.println(SQL_EXEPTION+"\n\t Can`t get statment");
        }
        return null;
    }

    //"select * from customers"
    public String ExecuteQuery(String query){
        if(connection==null) return null;
        String res="";
        try {
            result_set = statement.executeQuery(query);
            while(result_set.next()){
                res+=result_set.getCursorName();
            }
        } catch (SQLException e) {
            System.out.println(SQL_EXEPTION+"\n\t Can`t execute query");
        }
        return res;
    }

    public void closeConnection(){
        try {
            if(statement!=null)
                statement.close();
            if(connection!=null)
                connection.close();
        } catch (SQLException e) {
            System.out.println(SQL_EXEPTION+"\n\t Error on closing");
        }
    }

}
