package www.dubbinary;

/**
 * Created by dubbinary on 20.02.15.
 */

import www.dubbinary.connect.JDBC_Connection;

import java.sql.*;

public class JDBC_Tester {

    private final static String SQL_EXEPTION = "[ERROR]: SQL Exeption: ";

    private static Connection connection = null;
    private static Statement statement = null;
    private static PreparedStatement prepared_statement = null;
    private static ResultSet result_set = null;

    public static void main(String[] args) {
        JDBC_Connection s_con = new JDBC_Connection("jdbc:mysql://localhost:3306", "dubbinary", "454566");
        connection = s_con.getConnection();
        statement = s_con.getStatement();
        System.out.println(executeQuery("show databases"));
        s_con.closeConnection();


        System.out.print("\nout");
    }

    public static String executeQuery(String query){
        if(connection==null) return null;
        String res="";
        try {
            result_set = statement.executeQuery(query);
            System.out.println(result_set.next());
//            while(result_set.next()){                     //there
//                res+=result_set.getCursorName();
//            }
        } catch (SQLException e) {
            System.out.println(SQL_EXEPTION+"\n\t Can`t execute query");
        }
        return res;
    }
}
